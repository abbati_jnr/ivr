# -*-coding: utf-8 -*-

from chatterbot import ChatBot
import time
import pickle
import json
import os.path
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions


def wait():
    while True:
        try:
            e = WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.ID, 'app')))
            # print e
            return
        except:
            pass

def add_cookies():
    if os.path.isfile('cookies'):
        # cookies = pickle.load(open("cookies.pkl", "rb"))
        # for cookie in cookies:
        #     driver.add_cookie(cookie)

        with open('cookies') as j_load:
            cookies = json.load(j_load)
            for cookie in cookies:
                driver.add_cookie(cookie)

    else:
        # pickle.dump(driver.get_cookies(), open('cookies.pkl', 'wb'))
        cookies = driver.get_cookies()
        with open('cookies', 'w') as j_in:
            json.dump(cookies, j_in)




def extract_number():
    number = driver.find_element_by_id('main').find_element_by_tag_name('header').find_element_by_class_name('_1WBXd').find_element_by_tag_name('span').text
    print number
    return number

def extract_message():
    first_div = driver.find_element_by_id('main').find_element_by_class_name('_3zJZ2').find_element_by_class_name('copyable-area').find_element_by_class_name('_9tCEa')
    msg_divs = first_div.find_elements_by_xpath('./div')
    last_div = msg_divs[len(msg_divs) - 1]
    message = last_div.find_element_by_class_name('copyable-text').find_element_by_tag_name('span').text
    print message
    return message

def send_message(msg):
    txt_area = driver.find_element_by_id('main').find_element_by_tag_name('footer').find_element_by_class_name('_3pkkz').find_element_by_class_name('copyable-text')
    txt_area.send_keys(msg + '\n')

def get_reply(msg):
    # msg = raw_input("Me: ")
    res = chatbot.get_response(msg)
    print 'Hauwa: ' + str(res)
    return str(res)



chatbot = ChatBot(
    'Hauwa',
    # read_only=True,
    logic_adapters=[
        {
            'import_path': 'chatterbot.logic.BestMatch'
        },
        {
            'import_path': 'chatterbot.logic.LowConfidenceAdapter',
            'threshold': 0.6,
            'default_response': ''
        }
    ],
    trainer='chatterbot.trainers.ChatterBotCorpusTrainer'
)

chatbot.train('chatterbot.corpus.english.conversations')


# while True:
#     msg = raw_input("Me: ")
#     res = chatbot.get_response(msg)
#     print 'Hauwa: ' + str(res)


options = webdriver.ChromeOptions()
options.add_argument('--user-data-dir=./User_Data')
driver = webdriver.Chrome(executable_path='./chromedriver', chrome_options=options)
driver.get('https://web.whatsapp.com/')


# driver = webdriver.Chrome(executable_path='./chromedriver')
# executor_url = driver.command_executor
# session_id = driver.session_id
# driver.get('https://web.whatsapp.com/')

# driver2 = webdriver.Remote(command_executor=executor_url, desired_capabilities={})
# driver2.session_id = session_id
# print driver2.current_url

# driver3 = webdriver.Remote(command_executor=executor_url, desired_capabilities={})
# driver3.session_id = session_id

# time.sleep(5)


# notificatoins = driver2.find_element_by_id('side').find_element_by_id('pane-side').find_elements_by_class_name('OUeyt')
# print len(notificatoins)
#
# notificatoins = driver3.find_element_by_id('side').find_element_by_id('pane-side').find_elements_by_class_name('OUeyt')
# print len(notificatoins)
#
# c2 = driver2.find_elements_by_class_name('_3j7s9')
# print len(c2)
# c2[0].click()
# time.sleep(2)
# c3 = driver3.find_elements_by_class_name('_3j7s9')
# print len(c3)
# c3[len(c3) - 1].click()
#
# driver2.find_element_by_class_name('_3j7s9').click()
# time.sleep(5)
# driver3.find_element_by_class_name('_3j7s9').click()
#
# notifications = driver.find_element_by_id('side').find_element_by_id('pane-side').find_elements_by_class_name('OUeyt')
# print len(notifications)
# if notifications:
#     for notification in notifications:
#         notification.find_element_by_xpath("../../../..").click()
# notifications[0].find_element_by_xpath("../../../..").click()



while True:
    try:
        notifications = driver.find_element_by_id('side').find_element_by_id('pane-side').find_elements_by_class_name('OUeyt')
        # print len(notifications)
        if notifications:
            for notification in notifications:
                notification.find_element_by_xpath("../../../..").click()
                time.sleep(3)

                number = extract_number()

                message = extract_message()

                if len(message.split()) < 20:

                    reply = get_reply(message)

                    send_message(reply)


    except:
        pass


